module notabug.org/apiote/next-eeze-agent

go 1.15

require (
	git.sr.ht/~sircmpwn/go-bare v0.0.0-20200812160916-d2c72e1a5018
	github.com/sevlyar/go-daemon v0.1.5
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
)
